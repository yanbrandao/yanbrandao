import { Component, OnInit } from '@angular/core';
import {Messages} from '../../core/messages';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  getText(key: string) {
    return Messages.getMessage(key);
  }

}
