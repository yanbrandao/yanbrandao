import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Messages } from '../../core/messages';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit {

  @ViewChild('audioOption', {static: false}) audioPlayerRef: ElementRef;

  constructor() {
  }

  playAudio() {
    this.audioPlayerRef.nativeElement.play();
    console.log('clicked');
  }

  ngOnInit() {
  }

  getText(key: string) {
    return Messages.getMessage(key);
  }

}
