import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectListComponent } from './project/project-list/project-list.component';
import { KnowledgeListComponent } from './knowledge/knowledge-list/knowledge-list.component';
import { GameListComponent } from './game/game-list/game-list.component';
import {HomePageComponent} from './home/home-page/home-page.component';

const routes: Routes = [
  {path: '', component: HomePageComponent, data: {animation: 'home'}},
  {path: 'game', component: GameListComponent, data: {animation: 'game'}},
  {path: 'projects', component: ProjectListComponent, data: {animation: 'projects'}},
  {path: 'knowledge', component: KnowledgeListComponent, data: {animation: 'knowledge'}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
