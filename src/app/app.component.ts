import {Component, OnInit} from '@angular/core';
import { Messages } from './core/messages';
import {fadeAnimation, slideInAnimation} from './core/animation';
import {RouterOutlet} from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ slideInAnimation, fadeAnimation ]
})
export class AppComponent implements OnInit {
  title = 'yanbrandao';
  currentState = 'initial';
  breakpoint = null;

  getAnimationData(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  fadeFooter() {
    this.currentState = this.currentState === 'initial' ? 'final' : 'initial';
  }

  ngOnInit(): void {
    Messages.setLang('pt');
    this.breakpoint = (window.innerWidth <= 400) ? 1 : 6;
  }

  setLanguage(language: string) {
    Messages.setLang(language);
  }

  getText(key: string) {
    return Messages.getMessage(key);
  }

  onResize(event) {
    if (event.target.innerWidth <= 600) {
      this.breakpoint = 1;
    } else if (event.target.innerWidth > 600 && event.target.innerWidth <= 1100) {
      this.breakpoint = 3;
    } else {
      this.breakpoint = 5;
    }
  }
}
