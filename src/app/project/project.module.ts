import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectListComponent } from './project-list/project-list.component';
import {SharedModule} from '../shared/shared.module';
import { ProjectMesComponent } from './project-mes/project-mes.component';

@NgModule({
  declarations: [ProjectListComponent, ProjectMesComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class ProjectModule { }
