import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectMesComponent } from './project-mes.component';

describe('ProjectMesComponent', () => {
  let component: ProjectMesComponent;
  let fixture: ComponentFixture<ProjectMesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectMesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
