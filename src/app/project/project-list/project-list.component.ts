import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Messages} from '../../core/messages';

export interface Tile {
  logo: string;
  cols: number;
  rows: number;
  title: string;
  subtitle: string;
  text: string;
  app: string;
  route: string;
}

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {
  unityURL = '../../../assets/img/unity-logo.png';
  androidURL = '../../../assets/img/android-logo.png';
  angularURL = '../../../assets/img/angular-logo.png';

  tiles: Tile[] = [
    {title: 'MES', subtitle: 'Aplicação Multiplataforma',
      logo: '../../../assets/img/mes-prod.gif', cols: 1, rows: 1, app: this.angularURL,
      text: this.getText('MES_TEXT'),
      route: ''},
    {title: 'Devika', subtitle: 'Jogo 2D Unity',
      logo: '../../../assets/img/devika.gif', cols: 1, rows: 1,
      text: this.getText('DEVIKA_TEXT'), app: this.unityURL,
      route: ''},
    {title: 'OPA', subtitle: 'Obra pelo Artista',
      logo: '../../../assets/img/opa-icon.png',
      cols: 1, rows: 1, app: this.unityURL,
      text: this.getText('OPA_TEXT'),
      route: ''},
    {title: 'Upiara', subtitle: 'Jogo 2D Unity',
      logo: '../../../assets/img/upiara.png', cols: 1, rows: 1,
      text: this.getText('UPIARA_TEXT'), app: this.unityURL,
      route: ''},
    {title: 'Monkey Bird', subtitle: 'Unity 2D Game',
      logo: '../../../assets/img/mkb-background.png',
      cols: 1, rows: 1, app: this.unityURL,
      text: this.getText('MONKEY_TEXT'),
      route: ''}
  ];
  @ViewChild('audioOption', {static: false}) audioPlayerRef: ElementRef;

  constructor() {
  }

  playAudio() {
    this.audioPlayerRef.nativeElement.play();
    console.log('clicked');
  }
  ngOnInit() {
  }

  getText(key: string) {
    return Messages.getMessage(key);
  }
}
