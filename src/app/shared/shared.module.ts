import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatIconModule,
  MatListModule,
  MatAutocompleteModule,
  MatCheckboxModule,
  MatCardModule,
  MatGridListModule,
  MatTabsModule,
  MatButtonModule
} from '@angular/material';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import { NavbarComponent } from './navbar/navbar.component';
import { LayoutModule } from '@angular/cdk/layout';


@NgModule({
  declarations: [NavbarComponent],
  imports: [
    BrowserAnimationsModule,
    FlexLayoutModule,
    FlexModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatMenuModule,
    MatSidenavModule,
    MatTabsModule,
    MatButtonModule,
    MatGridListModule,
    CommonModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    FlexLayoutModule,
    MatListModule,
    LayoutModule,
    RouterModule
  ],
  exports: [
    BrowserAnimationsModule,
    MatCardModule,
    MatGridListModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatMenuModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatButtonModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    NavbarComponent,
    RouterModule
  ]
})
export class SharedModule { }
