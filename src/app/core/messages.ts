export class Messages {
    static lang = 'pt';
    private static messages = Messages.setLang('en');

    static setLang(lang: string) {
      this.lang = lang;
      if (this.lang === 'en') {
        this.messages = require('../../locale/messages.en-US.json');
      } else if (this.lang === 'pt') {
        this.messages = require('../../locale/messages.pt-BR.json');
      }
    }

    public static getMessage(key: string): string {
      return this.messages[key];
    }
  }
