import {
  trigger, animateChild, group,
  transition, animate, style, query, state, stagger
} from '@angular/animations';


export const fadeAnimation =
  trigger('fadeAnimation', [

    // the "in" style determines the "resting" state of the element when it is visible.
    state('initial', style({opacity: 1})),
    state('final', style({opacity: 0})),

    transition('*=>final', [
      query('.footer-fixed', [
        stagger(-5, [
          style({opacity: 1}),
          animate('300ms')
        ])
        ]
      )]),
    transition('*=>initial', [
      query('.footer-fixed', [
          stagger(-5, [
            style({opacity: 0}),
            group([
              animate('300ms', style({opacity: 1}))
            ])
          ])
        ]
      )]),

  ]);

// Routable animations
export const slideInAnimation =
  trigger('routeAnimation', [
    transition('* <=> *', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%'
        })
      ]),
      query(':enter', [
        style({ left: '-100%'})
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [
          animate('450ms ease-out', style({ left: '100%'}))
        ]),
        query(':enter', [
          animate('450ms ease-out', style({ left: '0%'}))
        ])
      ]),
      query(':enter', animateChild()),
    ])
  ]);


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
