import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {Messages} from '../../core/messages';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-knowledge-list',
  templateUrl: './knowledge-list.component.html',
  styleUrls: ['./knowledge-list.component.scss'],
  animations: [
    trigger('itemState', [
      state('in', style({
        opacity: 1
      })),
      state('out', style({
        opacity: 0.5
      })),
      transition('out => in', animate('1s ease-in-out')),
      transition('out => in', animate('1s ease-in-out'))
    ])
  ]
})
export class KnowledgeListComponent implements OnInit {
  title = 'yan-tapajos';
  currentCard = 0;
  card = [];
  isClicked = false;
  @ViewChild('audioOption', {static: false}) audioPlayerRef: ElementRef;

  constructor(
    private route: ActivatedRoute
  ) { }

  getText(key: string) {
    return Messages.getMessage(key);
  }

  ngOnInit() {
    this.card.push('.NET Framework');
    this.card.push('Unity3D');
    this.card.push('Angular 7');
    this.card.push('Java 8');
    this.card.push('Spring Boot');
    this.card.push('Sass');
    this.card.push('Docker');
    this.card.push('Jenkins');
    this.card.push('SonarQube');
  }

  changeLeft() {
    this.clicked();
    if (this.currentCard > 1) {
      this.currentCard -= 3;
    }
  }

  playAudio() {
    this.audioPlayerRef.nativeElement.play();
    console.log('clicked');
  }

  changeRight() {
    this.clicked();
    if (this.currentCard + 3 < this.card.length) {
      this.currentCard += 3;
    }
  }

  clicked() {
    this.isClicked = !this.isClicked;
  }

  animationFinished(event) {
    this.clicked();
  }

  animStart(event) {
    console.log('Animation Started');
    // do more stuff
  }
}
