import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KnowledgeListComponent } from './knowledge-list/knowledge-list.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [KnowledgeListComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class KnowledgeModule { }
